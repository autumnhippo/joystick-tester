# Joystick Tester

<a href="https://autumnhippo.com/products/joystick-tester"><img align="right" src="https://gitlab.com/autumnhippo/joystick-tester/-/raw/master/media/buy.png"></a>

Simple PCB to test Commodore 64/Amiga style joysticks. Connect to power via USB charger,
or attach 3 AA-batteries, and test if your vintage joystick still works as intended.

Includes a switch that lets you choose whether you want to send current on to the joystick
or not (you may not want to if your power source is not 5V, and also depending on the joystick).

Easy to assemble for the lightly skilled solderer.

Either have the PCB printed from the Gerber files and source the rest of the parts as per the list below, or
[buy it from the webshop](https://autumnhippo.com/products/joystick-tester).

![Assembled board](https://gitlab.com/autumnhippo/joystick-tester/-/raw/master/media/assembledboard.jpg "Assembled board")

## Parts List

* The PCB: Order it through your favourite PCB service by sending them the newest version of the gerber files.
* 5 x LEDs. Although the board is intended for 5 mm LEDs, you can use any size, form or colour you like.
* 5 x thrugh-hole resistors. For use with 5V (e.g. through USB), somewhere between 680 Ω to 2 KΩ is suggested,
  but it really depends on your LEDs, and how brightly you want them to be lit. Even 680 Ω is probably a relatively
  high value for most LEDs.
* 1 x male DB9 (DE-9) angled connector with horizontal pin pitch of 2.54 mm. Make sure the pins are one-to-one
  on the solder end (seen from above) with the connector end (seen from the plug end). I use re-purposed
  connectors from joystick extension cords, whose male end plug actually contains a PCB-mountable connector.
* 1 x mini-USB type B female connector for through-hole PCB-mounting. Designed for TE Connectivity 2172034-1
  (Mouser 571-2172034-1). Similar connectors can be found on Ebay and other sites, often as "Mini USB Type B
  Female DIP 90° Angled Board PCB Socket" or similar. This connector is not necessary if you intend to use
  batteries instead.
* 1 x mini-USB cable, for powering through USB.
* 1 x slide switch, 2-position, SPDT On-On, 3(5) pins, 2 mm pin pitch, e.g. Mouser 611-OS102011MA1QN1.
* optionally, 1 x battery holder for three AA or AAA battery cells. Or some other type of 5V battery supply. If you don't
  intend on supplying the tested joystick with 5V, you may of course use any voltage (e.g. from only two or even
  one AA cell) with the appropriate resistors. A battery holder is obviously not necessary if you intend to run
  on USB-power.

![PCB](https://gitlab.com/autumnhippo/joystick-tester/-/raw/master/media/pcb.png "PCB")

## Assembly Instructions

Simply solder all the parts to the board, starting from the lowest ones, working towards the taller parts.
Make sure to orient the LEDs correctly (the longer leg goes to +, or the flat side of the LED goes towards the
flat side of the silkscreen circle).

If you are going to use batteries, any holder will do. The board was not designed for any specific type of holder,
so some hotglue on the back side of the board will take you a long way. You may use the hole in the board next
to the power solder pads to route the wires from the battery holder.

If you are planning on not powering the board with 5V, I highly recommend *not* to mount the slide switch in order
to prevent accidentally sending incorrect voltage to the joystick (which may or may not have logic that can be
damaged). If you plan on powering through USB, you should be fine.

The board is equipped with 3 mm screw holes for machine screws, spacers, etc. in each corner to facilitate some
sort of casing, as well as measurements on the back-side, in case someone wanted to be creative with a 3D printer
or the like. If you do make a casing or any other changes/additions, *please* let me know how it goes!

