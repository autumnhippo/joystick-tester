<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Thomas">
<packages>
<package name="LED_5MM">
<pad name="A" x="-1.27" y="0" drill="1"/>
<pad name="C" x="1.27" y="0" drill="1" shape="square"/>
<wire x1="2.413" y1="-1.143" x2="2.413" y2="1.143" width="0.127" layer="21" curve="-309.093859"/>
<wire x1="2.413" y1="1.143" x2="2.413" y2="-1.143" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0" layer="39"/>
<text x="0" y="1.27" size="0.8128" layer="25" align="center">&gt;NAME</text>
<wire x1="-1.27" y1="-1.651" x2="-1.27" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-1.651" y1="-1.27" x2="-0.889" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.889" y1="-1.27" x2="1.651" y2="-1.27" width="0.127" layer="21"/>
</package>
<package name="USB_MINI-B_5PF_90DEG_THRUHOLE">
<description>Mini USB Type B 5PF
Female Connector Receptacle SMT/DIP 90°Angled/Tail Board PCB Socket
Ebay/China</description>
<pad name="3" x="0" y="0" drill="0.7" diameter="1.016"/>
<pad name="4" x="0.8" y="-1.2" drill="0.7" diameter="1.016"/>
<pad name="5" x="1.6" y="0" drill="0.7" diameter="1.016"/>
<pad name="2" x="-0.8" y="-1.2" drill="0.7" diameter="1.016"/>
<pad name="1" x="-1.6" y="0" drill="0.7" diameter="1.016"/>
<pad name="SHIELD@1" x="-3.65" y="-5.6" drill="1.8"/>
<pad name="SHIELD@2" x="3.65" y="-5.6" drill="1.8"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-9.525" x2="3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="3.81" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="0" width="0.127" layer="21"/>
<rectangle x1="-3.81" y1="-9.525" x2="3.81" y2="0" layer="39"/>
<text x="5.08" y="-8.89" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="DE-9_MALE_CONN_ANG_PCB">
<description>DE-9 Male Connector
Angled PCB thruhole mount, from joystick extender cable</description>
<pad name="1" x="-5.08" y="0" drill="1.4" diameter="1.778"/>
<pad name="2" x="-2.54" y="0" drill="1.4" diameter="1.778"/>
<pad name="3" x="0" y="0" drill="1.4" diameter="1.778"/>
<pad name="4" x="2.54" y="0" drill="1.4" diameter="1.778"/>
<pad name="5" x="5.08" y="0" drill="1.4" diameter="1.778"/>
<pad name="6" x="-3.81" y="-2.54" drill="1.4" diameter="1.778"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.4" diameter="1.778"/>
<pad name="8" x="1.27" y="-2.54" drill="1.4" diameter="1.778"/>
<pad name="9" x="3.81" y="-2.54" drill="1.4" diameter="1.778"/>
<wire x1="-7.62" y1="0" x2="7.62" y2="0" width="0.127" layer="21"/>
<wire x1="-9.525" y1="-24.13" x2="-9.525" y2="-15.24" width="0.127" layer="21"/>
<wire x1="9.525" y1="-15.24" x2="9.525" y2="-24.13" width="0.127" layer="21"/>
<wire x1="9.525" y1="-24.13" x2="-9.525" y2="-24.13" width="0.127" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-15.24" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-15.24" x2="-9.525" y2="-15.24" width="0.127" layer="21"/>
<wire x1="7.62" y1="0" x2="7.62" y2="-15.24" width="0.127" layer="21"/>
<wire x1="7.62" y1="-15.24" x2="9.525" y2="-15.24" width="0.127" layer="21"/>
<text x="0" y="-5.08" size="1.27" layer="25" align="top-center">&gt;NAME</text>
<rectangle x1="-9.525" y1="-24.13" x2="9.525" y2="-15.24" layer="39"/>
<rectangle x1="-7.62" y1="-15.24" x2="7.62" y2="0" layer="39"/>
<wire x1="-6.1468" y1="-2.032" x2="-6.1468" y2="-15.748" width="0" layer="20"/>
<wire x1="-6.1468" y1="-15.748" x2="-7.747" y2="-15.748" width="0" layer="20"/>
<wire x1="-7.747" y1="-15.748" x2="-7.747" y2="-2.032" width="0" layer="20"/>
<wire x1="-7.747" y1="-2.032" x2="-6.1468" y2="-2.032" width="0" layer="20"/>
<wire x1="6.1468" y1="-2.032" x2="7.747" y2="-2.032" width="0" layer="20"/>
<wire x1="7.747" y1="-2.032" x2="7.747" y2="-15.748" width="0" layer="20"/>
<wire x1="6.1468" y1="-15.748" x2="6.1468" y2="-2.032" width="0" layer="20"/>
<wire x1="7.747" y1="-15.748" x2="6.1468" y2="-15.748" width="0" layer="20"/>
</package>
<package name="RESISTOR_THRUHOLE">
<pad name="A" x="-3.81" y="0" drill="0.75"/>
<pad name="B" x="3.81" y="0" drill="0.75"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<text x="0" y="0" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-2.54" y1="-1.27" x2="2.54" y2="1.27" layer="39"/>
</package>
<package name="PWR_INPUT_DC_PADS">
<text x="-1.27" y="2.54" size="1.27" layer="25" rot="R90" align="center-left">&gt;VALUE</text>
<text x="1.27" y="2.54" size="1.27" layer="25" rot="R90" align="center-left">GND</text>
<pad name="VCC" x="-1.27" y="0" drill="0.9" shape="long" rot="R90"/>
<pad name="GND" x="1.27" y="0" drill="0.9" shape="long" rot="R90"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<text x="-2.921" y="0" size="1.27" layer="25" rot="R90" align="bottom-center">&gt;NAME</text>
</package>
<package name="PWR_INPUT_DC_PADS_ONESIDE">
<text x="-1.27" y="2.794" size="1.27" layer="25" rot="R90" align="center-left">&gt;VALUE</text>
<text x="1.27" y="2.794" size="1.27" layer="25" rot="R90" align="center-left">GND</text>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<text x="-2.921" y="0" size="1.27" layer="25" rot="R90" align="bottom-center">&gt;NAME</text>
<smd name="VCC" x="-1.27" y="0" dx="2.54" dy="1.27" layer="1" roundness="50" rot="R90"/>
<smd name="GND" x="1.27" y="0" dx="2.54" dy="1.27" layer="1" roundness="50" rot="R90"/>
</package>
<package name="USB_MINI-B_5PF_90DEG_THRUHOLE_TE">
<description>Mini USB Type B 5PF
Female Connector Receptacle SMT/DIP 90°Angled/Tail Board PCB Socket
TE Connectivity 2172034-1, Mouser 571-2172034-1
Also works with generic receptacle from ebay</description>
<pad name="3" x="0" y="0" drill="0.7" diameter="1.016"/>
<pad name="4" x="0.8" y="-1.2" drill="0.7" diameter="1.016"/>
<pad name="5" x="1.6" y="0" drill="0.7" diameter="1.016"/>
<pad name="2" x="-0.8" y="-1.2" drill="0.7" diameter="1.016"/>
<pad name="1" x="-1.6" y="0" drill="0.7" diameter="1.016"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-9.525" x2="3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="3.81" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="0" width="0.127" layer="21"/>
<rectangle x1="-3.81" y1="-9.525" x2="3.81" y2="0" layer="39"/>
<text x="0" y="-2.54" size="1.27" layer="25" align="top-center">&gt;NAME</text>
<wire x1="-4" y1="-4.1" x2="-4" y2="-6" width="0" layer="20"/>
<wire x1="-4" y1="-6" x2="-3.3" y2="-6" width="0" layer="20"/>
<wire x1="-3.3" y1="-6" x2="-3.3" y2="-4.1" width="0" layer="20"/>
<wire x1="-3.3" y1="-4.1" x2="-4" y2="-4.1" width="0" layer="20"/>
<wire x1="3.3" y1="-4.1" x2="3.3" y2="-6" width="0" layer="20"/>
<wire x1="4" y1="-6" x2="4" y2="-4.1" width="0" layer="20"/>
<wire x1="4" y1="-4.1" x2="3.3" y2="-4.1" width="0" layer="20"/>
<wire x1="3.3" y1="-6" x2="4" y2="-6" width="0" layer="20"/>
<wire x1="-3.81" y1="-6.8" x2="3.81" y2="-6.8" width="0.127" layer="51" style="shortdash"/>
<text x="0" y="-6.985" size="0.4064" layer="51" align="top-center">PCB edge</text>
<rectangle x1="2.54" y1="-6.35" x2="4.445" y2="-3.81" layer="16"/>
<rectangle x1="2.54" y1="-6.35" x2="4.445" y2="-3.81" layer="30"/>
<rectangle x1="-4.445" y1="-6.35" x2="-2.54" y2="-3.81" layer="30"/>
<rectangle x1="-4.445" y1="-6.35" x2="-2.54" y2="-3.81" layer="16"/>
<wire x1="4" y1="0.345" x2="3.3" y2="0.345" width="0" layer="20"/>
<wire x1="3.3" y1="0.345" x2="3.3" y2="-1.555" width="0" layer="20"/>
<wire x1="3.3" y1="-1.555" x2="4" y2="-1.555" width="0" layer="20"/>
<wire x1="4" y1="-1.555" x2="4" y2="0.345" width="0" layer="20"/>
<wire x1="-3.3" y1="0.345" x2="-4" y2="0.345" width="0" layer="20"/>
<wire x1="-4" y1="-1.555" x2="-3.3" y2="-1.555" width="0" layer="20"/>
<wire x1="-4" y1="0.345" x2="-4" y2="-1.555" width="0" layer="20"/>
<wire x1="-3.3" y1="-1.555" x2="-3.3" y2="0.345" width="0" layer="20"/>
<rectangle x1="2.54" y1="-1.905" x2="4.445" y2="0.635" layer="30"/>
<rectangle x1="2.54" y1="-1.905" x2="4.445" y2="0.635" layer="16"/>
<rectangle x1="-4.445" y1="-1.905" x2="-2.54" y2="0.635" layer="30"/>
<rectangle x1="-4.445" y1="-1.905" x2="-2.54" y2="0.635" layer="16"/>
</package>
<package name="TOGGLESWITCH_SPDT_90DEG_2MMPITCH">
<description>C&amp;K OS102011MA1QN1
(Mouser 611-OS102011MA1QN1)</description>
<pad name="A" x="-2" y="0" drill="0.9" shape="offset" rot="R270"/>
<pad name="N" x="0" y="0" drill="0.9" shape="offset" rot="R270"/>
<pad name="B" x="2" y="0" drill="0.9" shape="offset" rot="R270"/>
<wire x1="-4.3" y1="2.54" x2="4.3" y2="2.54" width="0.127" layer="21"/>
<wire x1="4.3" y1="2.54" x2="4.3" y2="-2.54" width="0.127" layer="21"/>
<wire x1="4.3" y1="-2.54" x2="-4.3" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-4.3" y1="-2.54" x2="-4.3" y2="2.54" width="0.127" layer="21"/>
<rectangle x1="-4.3" y1="-2.54" x2="4.3" y2="2.54" layer="39"/>
<text x="-5.08" y="-5.08" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="5.08" width="0.127" layer="25"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.127" layer="25"/>
<wire x1="0" y1="5.08" x2="0" y2="2.54" width="0.127" layer="25"/>
<pad name="GND@1" x="-4.1" y="0" drill="1.3" shape="square"/>
<pad name="GND@2" x="4.1" y="0" drill="1.3" shape="square"/>
<rectangle x1="-2.54" y1="2.54" x2="2.54" y2="6.35" layer="39"/>
</package>
<package name="TOGGLESWITCH_SPDT_90DEG_3MMPITCH">
<pad name="A" x="-3" y="0" drill="1" shape="offset" rot="R270"/>
<pad name="N" x="0" y="0" drill="1" shape="offset" rot="R270"/>
<pad name="B" x="3" y="0" drill="1" shape="offset" rot="R270"/>
<wire x1="-6.35" y1="6.35" x2="6.35" y2="6.35" width="0.127" layer="21"/>
<wire x1="6.35" y1="6.35" x2="6.35" y2="0" width="0.127" layer="21"/>
<wire x1="6.35" y1="0" x2="-6.35" y2="0" width="0.127" layer="21"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="6.35" width="0.127" layer="21"/>
<text x="-5.08" y="-5.08" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="10.16" width="0.127" layer="25"/>
<wire x1="-2.54" y1="10.16" x2="-1.27" y2="10.16" width="0.127" layer="25"/>
<wire x1="-1.27" y1="10.16" x2="-1.27" y2="6.35" width="0.127" layer="25"/>
<pad name="GND@1" x="-6" y="0" drill="1.3" shape="square"/>
<pad name="GND@2" x="6" y="0" drill="1.3" shape="square"/>
<rectangle x1="-3.175" y1="6.35" x2="3.175" y2="10.16" layer="39"/>
<rectangle x1="-6.35" y1="0" x2="6.35" y2="6.35" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<pin name="A" x="0" y="-7.62" visible="off" length="middle" rot="R90"/>
<pin name="C" x="0" y="7.62" visible="off" length="middle" rot="R270"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="5.08" size="1.778" layer="95" align="bottom-right">&gt;NAME</text>
<wire x1="-2.54" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.318" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="1.778" width="0.254" layer="94"/>
<wire x1="-3.302" y1="-1.27" x2="-5.842" y2="1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="1.27" x2="-5.842" y2="1.27" width="0.254" layer="94"/>
<wire x1="-5.842" y1="1.27" x2="-5.842" y2="0.508" width="0.254" layer="94"/>
</symbol>
<symbol name="DE-9">
<pin name="1" x="7.62" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pin" length="short" rot="R180"/>
<pin name="4" x="7.62" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="5" x="7.62" y="-5.08" visible="pin" length="short" rot="R180"/>
<pin name="6" x="-7.62" y="3.81" visible="pin" length="short"/>
<pin name="7" x="-7.62" y="1.27" visible="pin" length="short"/>
<pin name="8" x="-7.62" y="-1.27" visible="pin" length="short"/>
<pin name="9" x="-7.62" y="-3.81" visible="pin" length="short"/>
<wire x1="5.08" y1="7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<text x="-5.08" y="7.62" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="-0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<text x="0" y="1.27" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="PWR_INPUT_DC">
<circle x="0" y="0" radius="5.08" width="0.254" layer="94"/>
<wire x1="-1.016" y1="2.54" x2="1.016" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="3.556" x2="0" y2="1.524" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="1.016" y2="-2.54" width="0.254" layer="94"/>
<pin name="VCC" x="0" y="10.16" visible="off" length="middle" rot="R270"/>
<pin name="GND" x="0" y="-10.16" visible="off" length="middle" rot="R90"/>
<text x="-5.842" y="0" size="1.27" layer="96" align="center-right">&gt;VALUE</text>
</symbol>
<symbol name="TOGGLESWITCH_SPDT">
<circle x="2.54" y="5.08" radius="0.762" width="0.254" layer="94"/>
<pin name="N" x="-7.62" y="0" visible="off" length="middle"/>
<pin name="A" x="7.62" y="5.08" visible="off" length="middle" rot="R180"/>
<circle x="-2.54" y="0" radius="0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="3.556" y2="4.572" width="0.254" layer="94"/>
<circle x="2.54" y="-5.08" radius="0.762" width="0.254" layer="94"/>
<pin name="B" x="7.62" y="-5.08" visible="off" length="middle" rot="R180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95" align="bottom-right">&gt;NAME</text>
</symbol>
<symbol name="USB_CONN_NOSHIELD">
<pin name="1_VCC" x="-7.62" y="5.08" visible="pin"/>
<pin name="2_D-" x="-7.62" y="2.54" visible="pin"/>
<pin name="3_D+" x="-7.62" y="0" visible="pin"/>
<pin name="4_ID" x="-7.62" y="-2.54" visible="pin"/>
<pin name="5_GND" x="-7.62" y="-5.08" visible="pin"/>
<wire x1="-2.54" y1="6.35" x2="1.27" y2="7.62" width="0.254" layer="94"/>
<wire x1="1.27" y1="7.62" x2="1.27" y2="-7.62" width="0.254" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="-2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="10.16" x2="3.81" y2="-10.16" width="0.254" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-10.16" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="0" y2="10.16" width="0.254" layer="94"/>
<wire x1="0" y1="10.16" x2="3.81" y2="10.16" width="0.254" layer="94"/>
<text x="-5.08" y="10.668" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="GND">
<pin name="GND" x="0" y="0" visible="off" length="short" rot="R270"/>
<wire x1="-1.27" y1="-3.81" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-0.254" y1="-5.08" x2="0.254" y2="-5.08" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED_5MM" prefix="D">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM_THRUHOLE" package="LED_5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DE-9" prefix="CN">
<gates>
<gate name="G$1" symbol="DE-9" x="0" y="0"/>
</gates>
<devices>
<device name="MALE_CONN_ANG_PCB" package="DE-9_MALE_CONN_ANG_PCB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="THRUHOLE" package="RESISTOR_THRUHOLE">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PWR_INPUT_DC_PADS" prefix="PS" uservalue="yes">
<gates>
<gate name="G$1" symbol="PWR_INPUT_DC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PWR_INPUT_DC_PADS">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ONESIDE" package="PWR_INPUT_DC_PADS_ONESIDE">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB_NOSHIELD" prefix="CN">
<gates>
<gate name="G$1" symbol="USB_CONN_NOSHIELD" x="2.54" y="0"/>
</gates>
<devices>
<device name="KINA_2MNTHOLES" package="USB_MINI-B_5PF_90DEG_THRUHOLE">
<connects>
<connect gate="G$1" pin="1_VCC" pad="1"/>
<connect gate="G$1" pin="2_D-" pad="2"/>
<connect gate="G$1" pin="3_D+" pad="3"/>
<connect gate="G$1" pin="4_ID" pad="4"/>
<connect gate="G$1" pin="5_GND" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TE2172034-1" package="USB_MINI-B_5PF_90DEG_THRUHOLE_TE">
<connects>
<connect gate="G$1" pin="1_VCC" pad="1"/>
<connect gate="G$1" pin="2_D-" pad="2"/>
<connect gate="G$1" pin="3_D+" pad="3"/>
<connect gate="G$1" pin="4_ID" pad="4"/>
<connect gate="G$1" pin="5_GND" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TOGGLESWITCH_SPDT_90DEG_2MMPITCH" prefix="S">
<gates>
<gate name="G$2" symbol="GND" x="0" y="17.78"/>
<gate name="G$1" symbol="TOGGLESWITCH_SPDT" x="0" y="5.08"/>
</gates>
<devices>
<device name="" package="TOGGLESWITCH_SPDT_90DEG_2MMPITCH">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="N" pad="N"/>
<connect gate="G$2" pin="GND" pad="GND@1 GND@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="TOGGLESWITCH_SPDT_90DEG_3MMPITCH">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="N" pad="N"/>
<connect gate="G$2" pin="GND" pad="GND@1 GND@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.762" drill="0.508">
<clearance class="0" value="0.3048"/>
</class>
</classes>
<parts>
<part name="FIRE" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="UP" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="DOWN" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="LEFT" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="RIGHT" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="JOYSTICK" library="Thomas" deviceset="DE-9" device="MALE_CONN_ANG_PCB"/>
<part name="R1" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="680R"/>
<part name="R2" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="680R"/>
<part name="R3" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="680R"/>
<part name="R4" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="680R"/>
<part name="R5" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="680R"/>
<part name="POWER" library="Thomas" deviceset="PWR_INPUT_DC_PADS" device="ONESIDE" value="5V"/>
<part name="USB_POWER" library="Thomas" deviceset="USB_NOSHIELD" device="TE2172034-1"/>
<part name="VCC-TO-JS" library="Thomas" deviceset="TOGGLESWITCH_SPDT_90DEG_2MMPITCH" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="FIRE" gate="G$1" x="17.78" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="20.32" y="40.64" size="1.778" layer="95" rot="R180" align="bottom-right"/>
</instance>
<instance part="UP" gate="G$1" x="33.02" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="35.56" y="40.64" size="1.778" layer="95" rot="R180" align="bottom-right"/>
</instance>
<instance part="DOWN" gate="G$1" x="48.26" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="50.8" y="40.64" size="1.778" layer="95" rot="R180" align="bottom-right"/>
</instance>
<instance part="LEFT" gate="G$1" x="63.5" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="66.04" y="40.64" size="1.778" layer="95" rot="R180" align="bottom-right"/>
</instance>
<instance part="RIGHT" gate="G$1" x="78.74" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="81.28" y="40.64" size="1.778" layer="95" rot="R180" align="bottom-right"/>
</instance>
<instance part="JOYSTICK" gate="G$1" x="25.4" y="22.86" smashed="yes">
<attribute name="NAME" x="20.32" y="30.48" size="1.778" layer="95"/>
</instance>
<instance part="R1" gate="G$1" x="17.78" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="15.24" y="66.04" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="20.32" y="66.04" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R2" gate="G$1" x="33.02" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="30.48" y="66.04" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="35.56" y="66.04" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R3" gate="G$1" x="48.26" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="45.72" y="66.04" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="50.8" y="66.04" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R4" gate="G$1" x="63.5" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="60.96" y="66.04" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="66.04" y="66.04" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R5" gate="G$1" x="78.74" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="76.2" y="66.04" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="81.28" y="66.04" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="POWER" gate="G$1" x="-12.7" y="48.26" smashed="yes">
<attribute name="VALUE" x="-18.542" y="48.26" size="1.27" layer="96" align="center-right"/>
</instance>
<instance part="USB_POWER" gate="G$1" x="-35.56" y="48.26" smashed="yes">
<attribute name="NAME" x="-40.64" y="58.928" size="1.778" layer="95"/>
</instance>
<instance part="VCC-TO-JS" gate="G$2" x="5.08" y="33.02" smashed="yes"/>
<instance part="VCC-TO-JS" gate="G$1" x="5.08" y="35.56" smashed="yes" rot="R270">
<attribute name="NAME" x="7.62" y="38.1" size="1.778" layer="95" rot="R270" align="bottom-right"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="5V" class="0">
<segment>
<wire x1="-45.72" y1="81.28" x2="-12.7" y2="81.28" width="0.1524" layer="91"/>
<pinref part="POWER" gate="G$1" pin="VCC"/>
<wire x1="-12.7" y1="81.28" x2="-12.7" y2="58.42" width="0.1524" layer="91"/>
<junction x="-12.7" y="81.28"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="81.28" x2="5.08" y2="81.28" width="0.1524" layer="91"/>
<wire x1="5.08" y1="81.28" x2="17.78" y2="81.28" width="0.1524" layer="91"/>
<wire x1="17.78" y1="81.28" x2="17.78" y2="71.12" width="0.1524" layer="91"/>
<wire x1="17.78" y1="81.28" x2="33.02" y2="81.28" width="0.1524" layer="91"/>
<junction x="17.78" y="81.28"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="33.02" y1="81.28" x2="33.02" y2="71.12" width="0.1524" layer="91"/>
<wire x1="33.02" y1="81.28" x2="48.26" y2="81.28" width="0.1524" layer="91"/>
<junction x="33.02" y="81.28"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="48.26" y1="81.28" x2="48.26" y2="71.12" width="0.1524" layer="91"/>
<wire x1="48.26" y1="81.28" x2="63.5" y2="81.28" width="0.1524" layer="91"/>
<junction x="48.26" y="81.28"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="63.5" y1="81.28" x2="63.5" y2="71.12" width="0.1524" layer="91"/>
<wire x1="63.5" y1="81.28" x2="78.74" y2="81.28" width="0.1524" layer="91"/>
<junction x="63.5" y="81.28"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="78.74" y1="81.28" x2="78.74" y2="71.12" width="0.1524" layer="91"/>
<pinref part="USB_POWER" gate="G$1" pin="1_VCC"/>
<wire x1="-43.18" y1="53.34" x2="-45.72" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="53.34" x2="-45.72" y2="81.28" width="0.1524" layer="91"/>
<pinref part="VCC-TO-JS" gate="G$1" pin="N"/>
<wire x1="5.08" y1="43.18" x2="5.08" y2="81.28" width="0.1524" layer="91"/>
<junction x="5.08" y="81.28"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="-45.72" y1="20.32" x2="-12.7" y2="20.32" width="0.1524" layer="91"/>
<pinref part="JOYSTICK" gate="G$1" pin="8"/>
<wire x1="-12.7" y1="20.32" x2="10.16" y2="20.32" width="0.1524" layer="91"/>
<wire x1="17.78" y1="21.59" x2="10.16" y2="21.59" width="0.1524" layer="91"/>
<wire x1="10.16" y1="21.59" x2="10.16" y2="20.32" width="0.1524" layer="91"/>
<pinref part="POWER" gate="G$1" pin="GND"/>
<wire x1="-12.7" y1="38.1" x2="-12.7" y2="20.32" width="0.1524" layer="91"/>
<junction x="-12.7" y="20.32"/>
<pinref part="USB_POWER" gate="G$1" pin="5_GND"/>
<wire x1="-43.18" y1="43.18" x2="-45.72" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="43.18" x2="-45.72" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UP" class="0">
<segment>
<pinref part="JOYSTICK" gate="G$1" pin="1"/>
<wire x1="33.02" y1="27.94" x2="33.02" y2="38.1" width="0.1524" layer="91"/>
<pinref part="UP" gate="G$1" pin="C"/>
</segment>
</net>
<net name="DOWN" class="0">
<segment>
<pinref part="JOYSTICK" gate="G$1" pin="2"/>
<wire x1="33.02" y1="25.4" x2="48.26" y2="25.4" width="0.1524" layer="91"/>
<wire x1="48.26" y1="25.4" x2="48.26" y2="38.1" width="0.1524" layer="91"/>
<pinref part="DOWN" gate="G$1" pin="C"/>
</segment>
</net>
<net name="LEFT" class="0">
<segment>
<pinref part="JOYSTICK" gate="G$1" pin="3"/>
<wire x1="33.02" y1="22.86" x2="63.5" y2="22.86" width="0.1524" layer="91"/>
<wire x1="63.5" y1="22.86" x2="63.5" y2="38.1" width="0.1524" layer="91"/>
<pinref part="LEFT" gate="G$1" pin="C"/>
</segment>
</net>
<net name="RIGHT" class="0">
<segment>
<pinref part="JOYSTICK" gate="G$1" pin="4"/>
<wire x1="33.02" y1="20.32" x2="78.74" y2="20.32" width="0.1524" layer="91"/>
<wire x1="78.74" y1="20.32" x2="78.74" y2="38.1" width="0.1524" layer="91"/>
<pinref part="RIGHT" gate="G$1" pin="C"/>
</segment>
</net>
<net name="FIRE" class="0">
<segment>
<pinref part="JOYSTICK" gate="G$1" pin="6"/>
<wire x1="17.78" y1="26.67" x2="17.78" y2="38.1" width="0.1524" layer="91"/>
<pinref part="FIRE" gate="G$1" pin="C"/>
</segment>
</net>
<net name="R_FIRE" class="0">
<segment>
<pinref part="FIRE" gate="G$1" pin="A"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="17.78" y1="53.34" x2="17.78" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="R_UP" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="UP" gate="G$1" pin="A"/>
<wire x1="33.02" y1="60.96" x2="33.02" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="R_DOWN" class="0">
<segment>
<pinref part="DOWN" gate="G$1" pin="A"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="48.26" y1="53.34" x2="48.26" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="R_LEFT" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="LEFT" gate="G$1" pin="A"/>
<wire x1="63.5" y1="60.96" x2="63.5" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="R_RIGHT" class="0">
<segment>
<pinref part="RIGHT" gate="G$1" pin="A"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="78.74" y1="53.34" x2="78.74" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="JS_VCC" class="0">
<segment>
<pinref part="JOYSTICK" gate="G$1" pin="7"/>
<wire x1="17.78" y1="24.13" x2="10.16" y2="24.13" width="0.1524" layer="91"/>
<pinref part="VCC-TO-JS" gate="G$1" pin="A"/>
<wire x1="10.16" y1="24.13" x2="10.16" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="101,1,5.08,33.02,VCC-TO-JSG$2,GND,,,,"/>
</errors>
</schematic>
</drawing>
</eagle>
